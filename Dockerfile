FROM adoptopenjdk/openjdk11-openj9:jdk-11.0.6_10_openj9-0.18.1-ubuntu-slim
LABEL maintainer="hazsetata@gmail.com"

RUN apt-get update && \
    apt-get install -y --no-install-recommends libatomic1 ca-certificates wget curl build-essential zlib1g-dev && \
    rm -rf /var/lib/apt/lists/*

# Install GraalVM and native-image
RUN curl -L -o graalvm.tar.gz https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-19.3.1/graalvm-ce-java8-linux-amd64-19.3.1.tar.gz && \
    tar xvzf ./graalvm.tar.gz -C /opt && \
    rm ./graalvm.tar.gz && \
    /opt/graalvm-ce-java8-19.3.1/bin/gu install native-image

ENV GRAALVM_HOME=/opt/graalvm-ce-java8-19.3.1
ENV JAVA_TOOL_OPTIONS=
